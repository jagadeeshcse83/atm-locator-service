package com.jaga.atmlocatorservice.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class AtmLocatorResponseDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public Address address;
	public int distance;
	public List<OpeningHour> openingHours;
	public String functionality;
	public String type;
}