package com.jaga.atmlocatorservice.dto;

import java.io.Serializable;

public class Hour implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String hourFrom;
    public String hourTo;
}
