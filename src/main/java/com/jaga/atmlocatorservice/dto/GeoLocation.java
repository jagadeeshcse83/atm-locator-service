package com.jaga.atmlocatorservice.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class GeoLocation implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public String lat;
	public String lng;
}