package com.jaga.atmlocatorservice.dto;

import java.io.Serializable;
import java.util.List;

public class OpeningHour implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int dayOfWeek;
	public List<Hour> hours;
}