package com.jaga.atmlocatorservice.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class Address implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String street;
    public String housenumber;
    public String postalcode;
    public String city;
    public GeoLocation geoLocation;
}