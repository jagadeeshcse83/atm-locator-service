package com.jaga.atmlocatorservice.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jaga.atmlocatorservice.dto.AtmLocatorResponseDTO;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class AtmLocatorService {

	@Autowired
    private RestTemplate restTemplate;
    
    final String uri = "https://www.ing.nl/api/locator/atms/";
    
	/**
	 * 
	 * @return
	 */
	public ResponseEntity<Object> getAllAtms(){
		ResponseEntity<String> response = restTemplate.getForEntity(uri, String.class);
		String test = response.getBody().toString().replace(")]}',", "");// json from API is corrupted 
		ObjectMapper om = new ObjectMapper();
		AtmLocatorResponseDTO[] atmLocatorResponse = null;
		try {
			atmLocatorResponse = om.readValue(test, AtmLocatorResponseDTO[].class);
		} catch (JsonProcessingException e) {
			return new ResponseEntity<Object>("Exception encounter", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Object>(atmLocatorResponse, HttpStatus.OK);
	}
	
	/**
	 * 
	 * @param city
	 * @return
	 */
	public ResponseEntity<Object> getAllAtmsByCity(final String city) {
		List<AtmLocatorResponseDTO> atmLocatorByCityResponse = new ArrayList<>();
		ResponseEntity<Object> atmLocatorResponse = getAllAtms();
		if (!StringUtils.isBlank(city)) {
			if (atmLocatorResponse.getStatusCode().equals(HttpStatus.OK)) {
				AtmLocatorResponseDTO[] atms = (AtmLocatorResponseDTO[]) getAllAtms().getBody();
				for (AtmLocatorResponseDTO atm : atms) {
					if (atm.address.city.toLowerCase().contains(city.toLowerCase())) {
						atmLocatorByCityResponse.add(atm);
					}
				}
				return new ResponseEntity<Object>(atmLocatorByCityResponse, HttpStatus.OK);
			} else {
				return atmLocatorResponse;
			}
		} else {
			return new ResponseEntity<Object>("Invalid city name", HttpStatus.BAD_REQUEST);

		}

	}
	
}
