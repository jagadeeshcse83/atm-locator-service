package com.jaga.atmlocatorservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.jaga.atmlocatorservice.service.AtmLocatorService;

import lombok.extern.log4j.Log4j2;

@RestController
@Log4j2
public class AtmServiceLocatorAPIController {

	@Autowired
	AtmLocatorService atmLocatorService;

	@GetMapping("/atm")
	public ResponseEntity<?> getAllAtms() {
		return atmLocatorService.getAllAtms();
	}

	@GetMapping("/atm/{city}")
	public ResponseEntity<Object> getAllAtmsByCityName(@PathVariable("city") String city) {
		log.info("City " + city);
		return atmLocatorService.getAllAtmsByCity(city);
	}

}
